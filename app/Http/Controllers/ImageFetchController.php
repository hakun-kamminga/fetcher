<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImageFetchController.php extends Controller
{
    private $returnBody;

    public function getImages(Request $request)
    {
        $ch = curl_init();
        // Set the URL
        curl_setopt($ch, CURLOPT_URL, $request->url);
        // Set the HTTP method
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        // Return the response instead of printing it out
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // Send the request and store the result in $response
        $response = curl_exec($ch);
        curl_close($ch);
        return $this->filterImages($response);
    }
    /**
     * @param string $response
     * @return json
    */
    private function filterImages($response)
    {
        $doc = new DOMDocument();
        $doc->loadHTML($htmlString);
        $xpath = new DOMXPath($response);
        
        $images = $xpath->evaluate('img');
        return response()->json(['images' => $images], 200);
    }
}
