# README #

### What is this repository for? ###

* This is a simple example of how to fetch images from a given webpage using Vue and PHP.

### How do I get set up? ###
* Clone the repo
* Run composer install
* Run npm install

This app does not require a database.